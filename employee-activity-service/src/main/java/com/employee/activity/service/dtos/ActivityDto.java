package com.employee.activity.service.dtos;

import java.time.LocalDate;

import lombok.Data;
@Data
public class ActivityDto {
	private String status;
	private String description;
	private LocalDate date;

}
