package com.employee.activity.service.exceptions;
public class EmployeeNotFoundException extends Exception {

	public EmployeeNotFoundException(String message) {
		super(message);
	}
	
}