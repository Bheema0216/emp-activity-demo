package com.employee.activity.service.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.activity.service.dtos.EmployeeDto;
import com.employee.activity.service.exceptions.EmployeeNotFoundException;
import com.employee.activity.service.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;

	@PostMapping("/save")
	public String addEmployee(@Valid @RequestBody EmployeeDto employeeDto) {

		employeeService.addEmployee(employeeDto);
		return "Employee Details Added successfully";
	}

	@GetMapping("/")
	public List<EmployeeDto> viewEmployees() {
		return employeeService.viewEmployees();
	}

	@PatchMapping("/edit/{id}")
	public String editEmployee(@Valid @PathVariable Integer id, @Valid @RequestBody EmployeeDto employeeDto)
			throws EmployeeNotFoundException {
		employeeService.editEmployee(id, employeeDto);
		return "Employee Details edited succesfully";
	}

	@GetMapping("/{code}")
	public EmployeeDto viewEmployee(@Valid @PathVariable Integer code) throws EmployeeNotFoundException {
		return employeeService.viewEmployee(code);

	}

	@GetMapping("/getByName/{name}")
	public EmployeeDto findByName(@Valid @PathVariable String name) throws EmployeeNotFoundException {
		return employeeService.findByName(name);

	}
}