package com.employee.activity.service.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.employee.activity.service.dtos.DailyActivityDtoRequest;

import lombok.Data;

@Data
@Entity
public class Activity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private LocalDate date;

	private String description;

	private String status;

	@ManyToOne
	@JoinColumn(name = "emp_code", referencedColumnName = "code")
	private Employee employee;

	public Activity(DailyActivityDtoRequest request, Employee employee) {
		this.employee = employee;
		this.date = LocalDate.now();
		this.description = request.getDescription();
		this.status = request.getActivityStatus();
	}

	public Activity() {

	}

	/*
	 * public Activity( Employee employee) { this.employee=employee; }
	 */

	/*
	 * public Activity(DailyActivityDtoResponse response, Employee employee) {
	 * this.employee=employee; this.date=response.getDate();
	 * this.description=response.getDescription(); this.status =
	 * response.getStatus(); }
	 */
}