package com.employee.activity.service.serviceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.activity.service.dtos.ActivityDto;
import com.employee.activity.service.dtos.DailyActivityDtoRequest;
import com.employee.activity.service.dtos.DailyActivityDtoResponse;
import com.employee.activity.service.dtos.GetAllactivitiesDtoRequest;
import com.employee.activity.service.dtos.GetAllactivitiesDtoResponse;
import com.employee.activity.service.dtos.GetDailyActivitieswithNameAndDateRangeRequest;
import com.employee.activity.service.dtos.GetDailyActivitieswithNameAndDateRangeResponse;
import com.employee.activity.service.entity.Activity;
import com.employee.activity.service.entity.Employee;
import com.employee.activity.service.exceptions.ActivityNotFoundException;
import com.employee.activity.service.exceptions.EmployeeNotFoundException;
import com.employee.activity.service.repository.DailyActivityRepository;
import com.employee.activity.service.repository.EmployeeRespository;
import com.employee.activity.service.service.DailyActivityService;

@Service
public class DailyActivityServiceImpl implements DailyActivityService {

	@Autowired
	DailyActivityRepository dailyActivityRepository;

	@Autowired

	EmployeeRespository employeeRespository;

	@Override
	public void addDailyActivity(DailyActivityDtoRequest request) throws EmployeeNotFoundException {
		Activity activity = convertToEntity(request);
		dailyActivityRepository.save(activity);
	}

	private Activity convertToEntity(DailyActivityDtoRequest request) throws EmployeeNotFoundException {
		Employee employee = employeeRespository.findByName(request.getEmpName());
		if (employee == null) {
			throw new EmployeeNotFoundException("Employee record not found with name:" + request.getEmpName());
		}
		Activity activity = new Activity(request, employee);
		return activity;
	}

	@Override
	public void editDailyActivity(DailyActivityDtoRequest request)
			throws EmployeeNotFoundException, ActivityNotFoundException {
		Employee employee = employeeRespository.findByName(request.getEmpName());
		if (employee == null) {
			throw new EmployeeNotFoundException("Employee record not found with name:" + request.getEmpName());
		}

		Activity activity = dailyActivityRepository.findByDescription(request.getDescription());
		if (activity == null) {
			throw new ActivityNotFoundException("Activity record not found");
		}
		activity.setDate(LocalDate.now());
		activity.setDescription(request.getDescription());
		activity.setStatus(request.getActivityStatus());
		dailyActivityRepository.save(activity);
	}

	@Override
	public List<DailyActivityDtoResponse> getDailyActivitywithEmpCode(Integer code)
			throws EmployeeNotFoundException, ActivityNotFoundException {

		Employee employee = employeeRespository.findByCode(code);
		if (employee == null) {
			throw new EmployeeNotFoundException("Employee record not found with code:" + code);
		}
		List<Activity> activityList =dailyActivityRepository.findByEmployeeCode(code) ;
		
		if (activityList.isEmpty()) {
			throw new ActivityNotFoundException("Activity records not found for emp code :" + code);
		}
		List<DailyActivityDtoResponse> resultList = new ArrayList<DailyActivityDtoResponse>();
		for (Activity activity : activityList) {
			DailyActivityDtoResponse response = new DailyActivityDtoResponse();
			response.setDate(activity.getDate());
			response.setDescription(activity.getDescription());
			response.setEmailId(employee.getEmailId());
			response.setJobTitle(employee.getJobTitle());
			response.setName(employee.getName());
			response.setStatus(activity.getStatus());
			resultList.add(response);
		}

		return resultList;
	}

	@Override
	public List<GetAllactivitiesDtoResponse> viewDailyActivities(GetAllactivitiesDtoRequest request)
			throws ActivityNotFoundException {
		LocalDate fromDate = request.getFromDate();
		LocalDate toDate = request.getToDate();
		
		List<Activity> activites = dailyActivityRepository.findByDateBetween(fromDate, toDate);
		
		if (activites.isEmpty()) {
			throw new ActivityNotFoundException("no records found for your entered date range");
		}
		
		List<GetAllactivitiesDtoResponse> resultList = new ArrayList<GetAllactivitiesDtoResponse>();
		
		for (Activity activity : activites) {
			GetAllactivitiesDtoResponse response = new GetAllactivitiesDtoResponse();
			response.setStatus(activity.getStatus());
			response.setDate(activity.getDate());
			response.setDescription(activity.getDescription());
			response.setEmailId(activity.getEmployee().getEmailId());
			response.setJobTitle(activity.getEmployee().getJobTitle());
			response.setName(activity.getEmployee().getName());
			response.setCode(activity.getEmployee().getCode());

			resultList.add(response);
		}
		return resultList;
	}

	@Override
	public List<GetAllactivitiesDtoResponse> viewDailyActivity(LocalDate date)
			throws EmployeeNotFoundException, ActivityNotFoundException {
		List<Activity> activityList = dailyActivityRepository.findByDate(date);
		if (activityList.isEmpty()) {
			throw new ActivityNotFoundException("no records found for entered date");
		}

		List<GetAllactivitiesDtoResponse> resultList = new ArrayList<GetAllactivitiesDtoResponse>();

		for (Activity activity : activityList) {
			GetAllactivitiesDtoResponse response = new GetAllactivitiesDtoResponse();
			response.setStatus(activity.getStatus());
			response.setDate(activity.getDate());
			response.setDescription(activity.getDescription());
			response.setEmailId(activity.getEmployee().getEmailId());
			response.setJobTitle(activity.getEmployee().getJobTitle());
			response.setName(activity.getEmployee().getName());
			response.setCode(activity.getEmployee().getCode());

			resultList.add(response);
		}
		return resultList;
	}

	@Override
	public GetDailyActivitieswithNameAndDateRangeResponse viewDailyActivitieswithNameAndDateRange(
			GetDailyActivitieswithNameAndDateRangeRequest request) throws EmployeeNotFoundException, ActivityNotFoundException {

		Employee employee = employeeRespository.findByCode(request.getCode());
		if (employee == null) {
			throw new EmployeeNotFoundException("Employee record not found with code:" + request.getCode());
		}

		List<Activity> activityList = dailyActivityRepository.findByEmployeeCodeAndDateBetween(request.getCode(),
				request.getFromDate(), request.getToDate());
		
		if(activityList.isEmpty()) {
			throw new ActivityNotFoundException("no activity records found for your date range");
		}
		
		List<ActivityDto> activityDtoList = new ArrayList<ActivityDto>();

		for (Activity activity : activityList) {
			ActivityDto activityDto = new ActivityDto();
			activityDto.setDate(activity.getDate());
			activityDto.setStatus(activity.getStatus());
			activityDto.setDescription(activity.getDescription());

			activityDtoList.add(activityDto);
		}

		GetDailyActivitieswithNameAndDateRangeResponse response = new GetDailyActivitieswithNameAndDateRangeResponse();
		response.setActivityDto(activityDtoList);
		response.setName(employee.getName());
		response.setEmailId(employee.getEmailId());
		response.setJobTitle(employee.getJobTitle());
		

		return response;
	}

}
