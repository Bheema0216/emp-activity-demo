package com.employee.activity.service.service;

import java.util.List;

import com.employee.activity.service.dtos.EmployeeDto;
import com.employee.activity.service.exceptions.EmployeeNotFoundException;

public interface EmployeeService {

	void addEmployee(EmployeeDto employeeDto);

	List<EmployeeDto> viewEmployees();

	void editEmployee(Integer id, EmployeeDto employeeDto) throws EmployeeNotFoundException;

	EmployeeDto viewEmployee(Integer code) throws EmployeeNotFoundException;

	EmployeeDto findByName(String name) throws EmployeeNotFoundException;

}
