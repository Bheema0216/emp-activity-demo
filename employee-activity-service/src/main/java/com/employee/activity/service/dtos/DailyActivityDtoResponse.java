package com.employee.activity.service.dtos;

import java.time.LocalDate;

import lombok.Data;

@Data
public class DailyActivityDtoResponse {

	private String name;
	
	private String jobTitle;
	
	private String emailId;
	
	private String description;
	
	private String status;
	
	private LocalDate date;
	
	
	/*
	 * public DailyActivityDtoResponse(Activity activity, Employee emp) {
	 * this.setName(emp.getName()); this.setJobTitle(emp.getJobTitle());
	 * this.setEmailId(emp.getEmailId());
	 * this.setDescription(activity.getDescription());
	 * this.setStatus(activity.getStatus()); this.setDate(activity.getDate()); }
	 */
	
	public DailyActivityDtoResponse() {
		
	}
}
