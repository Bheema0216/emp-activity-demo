package com.employee.activity.service.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.activity.service.dtos.DailyActivityDtoRequest;
import com.employee.activity.service.dtos.DailyActivityDtoResponse;
import com.employee.activity.service.dtos.GetAllactivitiesDtoRequest;
import com.employee.activity.service.dtos.GetAllactivitiesDtoResponse;
import com.employee.activity.service.dtos.GetDailyActivitieswithNameAndDateRangeRequest;
import com.employee.activity.service.dtos.GetDailyActivitieswithNameAndDateRangeResponse;
import com.employee.activity.service.exceptions.ActivityNotFoundException;
import com.employee.activity.service.exceptions.EmployeeNotFoundException;
import com.employee.activity.service.service.DailyActivityService;

@RestController
@RequestMapping("/dailyactivity")
public class DailyActivityController {

	@Autowired
	DailyActivityService dailyActivityService;

	@PostMapping("/createNewActivity")
	public String addDailyActivity(@Valid @RequestBody DailyActivityDtoRequest request) throws EmployeeNotFoundException {
		dailyActivityService.addDailyActivity(request);
		return "DailyActivity Added successfully";
	}

	@GetMapping("/{code}")
	public List<DailyActivityDtoResponse> getDailyActivitywithEmpCode(@Valid @PathVariable Integer code)
			throws EmployeeNotFoundException, ActivityNotFoundException {
		return dailyActivityService.getDailyActivitywithEmpCode(code);
	}

	@PatchMapping("/edit")
	public String editDailyActivity(@Valid @RequestBody DailyActivityDtoRequest request)
			throws EmployeeNotFoundException, ActivityNotFoundException {
		dailyActivityService.editDailyActivity(request);
		return "DailyActivity edited succesfully";
	}

	@PostMapping("/getDailyActivitiesFromDateAndToDate")
	public List<GetAllactivitiesDtoResponse> viewDailyActivities(@Valid @RequestBody GetAllactivitiesDtoRequest request)
			throws ActivityNotFoundException {

		return dailyActivityService.viewDailyActivities(request);
	}

	@GetMapping("/byDate/{date}")
	public List<GetAllactivitiesDtoResponse> viewDailyActivity(@Valid
			@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date)
			throws EmployeeNotFoundException, ActivityNotFoundException {
		return dailyActivityService.viewDailyActivity(date);
	}
	@PostMapping("/byDateRangeAndCode")
	public GetDailyActivitieswithNameAndDateRangeResponse viewDailyActivitieswithNameAndDateRange(@Valid
			@RequestBody GetDailyActivitieswithNameAndDateRangeRequest request) throws EmployeeNotFoundException, ActivityNotFoundException {
		
		return dailyActivityService.viewDailyActivitieswithNameAndDateRange(request);

	}

}