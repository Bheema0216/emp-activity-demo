package com.employee.activity.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.activity.service.entity.Employee;

@Repository
public interface EmployeeRespository extends JpaRepository<Employee, Integer>{

	Employee findByCode(Integer code);

	Employee findByName(String name);
	
}