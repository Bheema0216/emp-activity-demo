package com.employee.activity.service.dtos;

import java.util.List;

import lombok.Data;

@Data
public class GetDailyActivitieswithNameAndDateRangeResponse {

	private String name;
	
	private String jobTitle;
	
	private String emailId;
	
	private List<ActivityDto> activityDto;
	
}
