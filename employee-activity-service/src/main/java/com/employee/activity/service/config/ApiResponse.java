package com.employee.activity.service.config;

import java.time.LocalDate;

import lombok.Data;
@Data
public class ApiResponse {
	private final boolean success;
	private final String message;
	
	public ApiResponse(boolean success, String message) {
		this.success = success;
		this.message = message;
	}
	public String getTimestamp() {
		return LocalDate.now().toString();
	}
}
