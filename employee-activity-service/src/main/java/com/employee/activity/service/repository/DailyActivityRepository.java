package com.employee.activity.service.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.activity.service.entity.Activity;

@Repository
public interface DailyActivityRepository extends JpaRepository<Activity, Integer> {

	Activity findByDescription(String description);

	List<Activity> findByDateBetween(LocalDate fromDate, LocalDate toDate);

	List<Activity> findByDate(LocalDate date);

	List<Activity> findByEmployeeCodeAndDateBetween(Integer code, LocalDate fromDate, LocalDate toDate);

	List<Activity> findByEmployeeCode(Integer code);

	// List<Activity> findByDateBetweenAndCode(Integer code, LocalDate fromDate,
	// LocalDate toDate); -->by this the JPA won't construct the Query

}