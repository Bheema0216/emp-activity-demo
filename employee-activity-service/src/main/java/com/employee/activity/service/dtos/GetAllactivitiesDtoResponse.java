package com.employee.activity.service.dtos;

import java.time.LocalDate;

import lombok.Data;

@Data
public class GetAllactivitiesDtoResponse {
	
	private Integer code;
	
	private String name;

	private String jobTitle;

	private String emailId;

	private String description;

	private LocalDate date;
	
	private String status;
}
