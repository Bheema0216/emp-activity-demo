package com.employee.activity.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.activity.service.dtos.EmployeeDto;
import com.employee.activity.service.entity.Employee;
import com.employee.activity.service.exceptions.EmployeeNotFoundException;
import com.employee.activity.service.repository.EmployeeRespository;
import com.employee.activity.service.service.EmployeeService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRespository employeeRespository;

	@Override
	public void addEmployee(EmployeeDto employeeDto) {
		Employee emp = convertToEntity(employeeDto);
		employeeRespository.save(emp);
	}

	// Method for conversion from EmployeeDto object to Employee object.
	private static Employee convertToEntity(EmployeeDto employeeDto) {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(employeeDto, new TypeReference<Employee>() {
		});
	}

	@Override
	public List<EmployeeDto> viewEmployees() {
		List<Employee> result = employeeRespository.findAll();
		List<EmployeeDto> resultList = new ArrayList<EmployeeDto>();
		for (Employee emp : result) {
			EmployeeDto empDto = new EmployeeDto();
			empDto.setCode(emp.getCode());
			empDto.setEmailId(emp.getEmailId());
			empDto.setExperience(emp.getExperience());
			empDto.setJobTitle(emp.getJobTitle());
			empDto.setLocation(emp.getLocation());
			empDto.setName(emp.getName());
			empDto.setPhoneNumber(emp.getPhoneNumber());
			empDto.setProjectStatus(emp.getProjectStatus());
			resultList.add(empDto);
		}
		return resultList;
	}

	@Override
	public void editEmployee(Integer id, EmployeeDto employeeDto) throws EmployeeNotFoundException {
		Employee employee = employeeRespository.findById(id).orElse(null);
		if (employee != null) {
			employee.setCode(employeeDto.getCode());
			employee.setEmailId(employeeDto.getEmailId());
			employee.setExperience(employeeDto.getExperience());
			employee.setJobTitle(employeeDto.getJobTitle());
			employee.setLocation(employeeDto.getLocation());
			employee.setName(employeeDto.getName());
			employee.setPhoneNumber(employeeDto.getPhoneNumber());
			employee.setProjectStatus(employeeDto.getProjectStatus());
			employeeRespository.save(employee);
		} else {
			throw new EmployeeNotFoundException("Emplyee not found with enterd id :" + id);
		}
	}

	@Override
	public EmployeeDto viewEmployee(Integer code) throws EmployeeNotFoundException {
		Employee employee = employeeRespository.findByCode(code);
		if (employee == null) {
			throw new EmployeeNotFoundException("Emplyee not found with enterd code :" + code);
		}

		EmployeeDto empDto = new EmployeeDto();
		empDto.setCode(employee.getCode());
		empDto.setEmailId(employee.getEmailId());
		empDto.setExperience(employee.getExperience());
		empDto.setJobTitle(employee.getJobTitle());
		empDto.setLocation(employee.getLocation());
		empDto.setName(employee.getName());
		empDto.setPhoneNumber(employee.getPhoneNumber());
		empDto.setProjectStatus(employee.getProjectStatus());
		return empDto;
	}

	@Override
	public EmployeeDto findByName(String name) throws EmployeeNotFoundException {
		Employee employee = employeeRespository.findByName(name);
		if (employee == null) {
			throw new EmployeeNotFoundException("Emplyee not found with enterd name :" + name);
		}
		EmployeeDto empDto = new EmployeeDto();
		empDto.setCode(employee.getCode());
		empDto.setEmailId(employee.getEmailId());
		empDto.setExperience(employee.getExperience());
		empDto.setJobTitle(employee.getJobTitle());
		empDto.setLocation(employee.getLocation());
		empDto.setName(employee.getName());
		empDto.setPhoneNumber(employee.getPhoneNumber());
		empDto.setProjectStatus(employee.getProjectStatus());
		return empDto;

	}

	// Method for conversion from Employee Entity object to EmployeeDto object.
	/*
	 * private static EmployeeDto convertToDto(Employee employee) { ObjectMapper
	 * mapper = new ObjectMapper();
	 * 
	 * mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES); return
	 * mapper.convertValue(employee, new TypeReference<EmployeeDto>() { }); }
	 */

}