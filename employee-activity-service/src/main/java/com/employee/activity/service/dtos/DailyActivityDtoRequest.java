package com.employee.activity.service.dtos;


import lombok.Data;

@Data
public class DailyActivityDtoRequest{
	
	
	private String empName;
	
	private String description;
	
	private String activityStatus;
}