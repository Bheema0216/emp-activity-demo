package com.employee.activity.service.dtos;

import java.time.LocalDate;

import lombok.Data;

@Data
public class GetDailyActivitieswithNameAndDateRangeRequest {

	private Integer code;
	
	private LocalDate fromDate;
	
	private LocalDate toDate;
}
