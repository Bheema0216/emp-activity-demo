package com.employee.activity.service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {

	private Integer code;

	private String name;

	private String jobTitle;

	private String emailId;

	private Integer experience;

	private Long phoneNumber;

	private String location;

	private String projectStatus;

}