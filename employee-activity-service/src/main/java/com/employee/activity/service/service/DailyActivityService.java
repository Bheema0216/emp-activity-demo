package com.employee.activity.service.service;

import java.time.LocalDate;
import java.util.List;

import com.employee.activity.service.dtos.DailyActivityDtoRequest;
import com.employee.activity.service.dtos.DailyActivityDtoResponse;
import com.employee.activity.service.dtos.GetAllactivitiesDtoRequest;
import com.employee.activity.service.dtos.GetAllactivitiesDtoResponse;
import com.employee.activity.service.dtos.GetDailyActivitieswithNameAndDateRangeRequest;
import com.employee.activity.service.dtos.GetDailyActivitieswithNameAndDateRangeResponse;
import com.employee.activity.service.exceptions.ActivityNotFoundException;
import com.employee.activity.service.exceptions.EmployeeNotFoundException;

public interface DailyActivityService {

	void addDailyActivity(DailyActivityDtoRequest request) throws EmployeeNotFoundException;

	List<DailyActivityDtoResponse> getDailyActivitywithEmpCode(Integer code)
			throws EmployeeNotFoundException, ActivityNotFoundException;

	void editDailyActivity(DailyActivityDtoRequest request) throws EmployeeNotFoundException, ActivityNotFoundException;

	List<GetAllactivitiesDtoResponse> viewDailyActivities(GetAllactivitiesDtoRequest request)
			throws ActivityNotFoundException;

	List<GetAllactivitiesDtoResponse> viewDailyActivity(LocalDate date)
			throws EmployeeNotFoundException, ActivityNotFoundException;

	GetDailyActivitieswithNameAndDateRangeResponse viewDailyActivitieswithNameAndDateRange(
			GetDailyActivitieswithNameAndDateRangeRequest request) throws EmployeeNotFoundException, ActivityNotFoundException;

}
