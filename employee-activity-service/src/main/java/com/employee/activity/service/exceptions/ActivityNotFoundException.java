package com.employee.activity.service.exceptions;

public class ActivityNotFoundException extends Exception {

	public ActivityNotFoundException(String message) {
		super(message);
	}
}
